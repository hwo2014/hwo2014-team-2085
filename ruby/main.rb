require 'json'
require 'socket'
require './queue.rb'
require './aux.rb'

server_host = ARGV[0]
server_port = ARGV[1]
# bot_name = ARGV[2]
bot_name = 'boomslang'
bot_key = ARGV[3]

tracks = ['keimola','germany','usa','france']

track_name = tracks[0]

password = '123'
car_count = 1

buffer_size = 5

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"


# Todo :
# Learn from crashes.
# Learn from other cars' crashes.

class Environment

  attr_accessor :last_curve_index, :car_width, :car_length, :glue_position, :bot_color, :drag_factor, :mass,  :max_centrifugal_force, :next_curve, :max_speed, :max_speed_next
  def initialize
    self.next_curve = {}
    self.last_curve_index = nil
  end

  def set_car_variables(data)
    self.car_length = data['length']
    self.car_width = data['width']
    self.glue_position = data['glueFlagPosition']
  end

end

class NoobBot

  def initialize(server_host, server_port, bot_name, bot_key, track_name, password, car_count, buffer_size)

    @environment = Environment.new
    @race_info = RaceInfo.new buffer_size
    @game_tick = 0
    @max_throttle = 1.0
    @min_throttle = 0.0
    @throttle = @max_throttle
    @track_name = track_name
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
    # create_race(bot_name, bot_key, track_name, password, car_count, tcp)

  end

  # private
  @game_tick = 0
  @race_info = nil
  @environment = nil

  @throttle = nil
  @max_throttle = nil

  @min_throttle = nil
  @car = nil
  def color
    @environment.bot_color
  end
  def position
    @car.position
  end

  # def car
  #   @race_info.cars[self.color]
  # end


  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def create_race(bot_name, bot_key, track_name, password, car_count, tcp)
    # puts create_race_message(bot_name, bot_key, track_name, password, car_count)
    tcp.puts create_race_message(bot_name, bot_key, track_name, password, car_count)
    react_to_messages_from_server tcp
  end


  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']

      case msgType
        when 'carPositions'
          @game_tick = message['gameTick'] if message['gameTick'] # TODO : CHECK IF CORRECT PLACE, DEBUG

          @race_info.update_positions(msgData)

          tcp.puts tick_response if @game_tick > 0

        else
          case msgType
            when 'gameInit'

              # File.write(@track_name+'_data.txt',JSON.pretty_generate(msgData))

              @race_info.set_race_info(msgData['race'])
              @environment.set_car_variables(msgData['race']['cars'][0])

              @car = @race_info.cars[self.color]

            when 'yourCar'
              @environment.bot_color = msgData['color']
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
              tcp.puts tick_response
            when 'crash'
              if msgData['color'] == self.color
                puts "We crashed: speed: #{@car.speed} radius: #{@race_info.track.lane_radius(position.piece_index,position.start_lane)}"

                calculate_max_centrifugal_force(@car.speed, @race_info.track.lane_radius(position.piece_index,position.start_lane)) # TODO: start_lane -> lane

              else
                puts 'Someone else crashed'
              end

            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
            else
              # tcp.puts ping_message
              puts 'This message is not handled!'

          end
          puts "Got #{msgType}"
          # tcp.puts ping_message
      end
    end
  end

  def tick_response
    @race_info.calculate_speed(self.color)

    @environment.next_curve = @race_info.track.next_curve(position.piece_index,position.in_piece_distance, position.start_lane) # TODO: start lane

    if !(@environment.max_speed_next) or !(@environment.last_curve_index) or @environment.next_curve[:index] > @environment.last_curve_index

      @environment.last_curve_index = @environment.next_curve[:index]

      @environment.max_speed = @environment.max_speed_next


      @environment.max_speed_next = max_speed_for_curve(@race_info.track.lane_radius(@environment.next_curve[:index],position.start_lane))
    end

    @environment.max_speed = nil unless @race_info.track.pieces[position.piece_index]['radius']


    if @game_tick < 2
      @throttle = 1.0
    elsif @game_tick == 2
      calculate_drag_factor(*(@car.speed_buffer[0..1]))
    elsif @game_tick == 3
      calculate_car_mass(*(@car.speed_buffer[0..1]))
    else
      # TODO: Compute the distance to fall to the next max curve speed with throttle 0.0,
      # if the distance is good continue, if it's just enough set throttle to 0.0.

      compute_throttle



    end



    bound_throttle

    puts "Track: #{@track_name} Tick: #{@game_tick} - Piece: #{position.piece_index} - In Piece D: #{position.in_piece_distance}  -  Angle: #{position.angle} - Speed: #{@car.speed} - Throttle : #{@throttle}"
    puts "Next curve in : #{@environment.next_curve[:distance]} - Max speed: #{@environment.max_speed } - Max speed next: #{@environment.max_speed_next}"

    throttle_message

  end


  def compute_throttle
    return @throttle = 1.0 unless @environment.max_speed

    while (speed_in_ticks(20) > @environment.max_speed - 0.05) and not bound_throttle
      @throttle -= 0.025
      bound_throttle
      puts "1 Throttle set to #{@throttle}, speed in 20 ticks #{speed_in_ticks(20)}, current speed: #{@car.speed}"
    end

    while (speed_in_ticks(20) < @environment.max_speed - 1.0) and not bound_throttle
      @throttle += 0.05
      bound_throttle
      puts "2 Throttle set to #{@throttle}, speed in 20 ticks #{speed_in_ticks(20)}, current speed: #{@car.speed}"
    end

    bound_throttle

  end

  def bound_throttle

    if @throttle >= @max_throttle
      @throttle = @max_throttle
      true
    elsif @throttle <= @min_throttle
      @throttle = @min_throttle
      true
    end
    false
  end


  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def create_race_message(bot_name, bot_key, track_name, password, car_count)
    make_msg('createRace',
             {:botId => {:name => bot_name, :key => bot_key},
              :trackName => track_name,
              :password => password,
              :carCount => car_count
             })
  end

  def throttle_message
    # JSON.generate({:msgType => 'throttle', :data => throttle, :gameTick => @game_tick}) # dangerous if you're out of sync
    make_msg("throttle", @throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def calculate_drag_factor(v2, v1)

    h = @throttle ? @throttle  : 1.0
    k = ( v1 - ( v2 - v1 ) ) / v1 / v1 * h
    @environment.drag_factor = k
    puts "Drag factor calculated: #{k}, v1: #{v1}, v2: #{v2}"
    k
  end


  def calculate_car_mass(v3, v2)
    k = @environment.drag_factor
    h = @throttle ? @throttle  : 1.0

    m = 1.0 / ( Math::log( ( v3 - ( h / k ) ) / ( v2 - ( h / k ) ) ) / ( -k ) )
    @environment.mass = m
    puts "Car mass calculated: #{m}, v2: #{v2}, v3: #{v3}"
    m
  end

  def calculate_max_centrifugal_force(v,r)
    begin
      c = v*v/r
      @environment.max_centrifugal_force = c if !@environment.max_centrifugal_force or c < @environment.max_centrifugal_force

      # File.open('centrifugal_force.txt', 'a') do |f|
      #   f.puts "Track: #{@track_name} Tick: #{@game_tick} -- Piece: #{position.piece_index} -- In Piece D: #{position.in_piece_distance} -- Angle: #{position.angle} -- Speed: #{@car.speed} -- Throttle : #{@throttle}
      #   -- Max centrifugal force calculated: #{c}, v: #{v}, r: #{r}"
      # end
      c

    rescue Exception
      puts $!, $@
      nil
    end

  end

  def max_speed_for_curve(r)
    @environment.max_centrifugal_force ? Math::sqrt(r*@environment.max_centrifugal_force) : nil
  end

  # The estimated speed in given ticks
  def speed_in_ticks(t, v0= @car.speed, h=@throttle, m=@environment.mass, k=@environment.drag_factor)
    begin
      (v0 - (h/k) ) * Math::E ** ( ( - k * t ) / m ) + ( h/k )
    rescue
      nil
    end
  end

  # The tick right before reaching the given speed.
  def ticks_to_speed(v, v0= @car.speed, h=@throttle, m=@environment.mass, k=@environment.drag_factor)
    begin
      t = ( Math::log( (v - ( h/k ) )/(v0 - ( h/k ) ) ) * m ) / ( -k )
      t.round
    rescue
      nil
    end

  end

  # The distance to be traveled in the given number of ticks. The result is a bit less than the actual distance.
  def distance_in_ticks(t, v0= @car.speed, h=@throttle, m=@environment.mass, k=@environment.drag_factor)
    begin
      ( m/k ) * ( v0 - ( h/k ) ) * ( 1.0 - Math::E ** ( ( -k*t ) / m ) ) + ( h/k ) * t
    rescue
      nil
    end
  end


end

NoobBot.new(server_host, server_port, bot_name, bot_key, track_name, password, car_count, buffer_size)
