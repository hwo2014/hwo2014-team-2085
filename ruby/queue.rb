class FixedSizedQueue

  include Enumerable

  def initialize(size)
    @size = size
    @queue = Array.new
  end

  def size
    @size
  end

  def each(&blk)
    @queue.each(&blk)
  end

  def pop
    @queue.pop
  end

  def push(value)
    @queue.pop if @queue.size >= @size
    @queue.unshift(value)
  end

  def to_a
    @queue
  end

  def <<(value)
    push(value)
  end
  def [](value)
    @queue[value]
  end

end