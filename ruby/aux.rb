module ErrorMessage

  def push_error(data)
    # File.open('error.txt', 'a') do |f|
    #   f.puts data
    # end
    puts 'Error : ' + data
  end

end


class Position
  attr_accessor :angle, :piece_index, :in_piece_distance, :start_lane, :end_lane, :lap

  def initialize(data)
    self.angle = data['angle']

    piece_pos = data['piecePosition']
    self.piece_index = piece_pos['pieceIndex']
    self.in_piece_distance = piece_pos['inPieceDistance']
    self.lap = piece_pos['lap']
    self.start_lane = piece_pos['lane']['startLaneIndex']
    self.end_lane = piece_pos['lane']['endLaneIndex']
  end

  def abs_angle
    self.angle.abs
  end
end

class RaceInfo

  include ErrorMessage

  attr_accessor :track, :cars, :race_session

  PI = 3.141592653589793

  @buffer_size = nil


  # Speed : Distance traveled in the last game tick.
  # TODO : Bug: When you pass to a new piece, the speed becomes negative.
  def calculate_speed(color)
    car = cars[color]

    return 0 if car.previous_position == nil

    p_index = car.previous_position.piece_index
    c_index = car.position.piece_index

    if p_index <= c_index
      pieces = track.pieces[p_index..c_index]
    else
      pieces = track.pieces.drop(p_index) + track.pieces[0,c_index+1]
    end

    # Note that we are not using the current piece information at all. This may change to handle lane-switching.

    distance = car.position.in_piece_distance - car.previous_position.in_piece_distance

    if pieces.length == 1
      car.speed = check_speed(distance, car) ? distance : car.speed
      car.speed_buffer.push car.speed
      return car.speed
    end


    pieces[0..(pieces.length-2)].each do |piece|
      if piece['length'] == nil
        distance += piece[car.position.start_lane] # TODO : Do not assume that we will not be changing lanes.
      else
        distance += piece['length']
      end
    end

    car.speed = check_speed(distance, car) ? distance : car.speed
    car.speed_buffer.push car.speed
    car.speed

  end

  def check_speed(speed, car)
    if speed > 20 or speed < 0
      push_error "Speed calculation error: Speed #{speed}, #{car.position.inspect}"
      return false
    end
    true
  end

  def initialize(buffer_size)
    @buffer_size = buffer_size
  end

  def set_race_info(data)
    self.track = Track.new(data['track'])
    self.race_session = data['raceSession']

    self.cars = {}

    data['cars'].each do |car_data|
      self.cars[car_data['id']['color']] = Car.new(car_data, @buffer_size)
    end

  end

  def update_positions(data)
    data.each do |item|
      cars[item['id']['color']].position_buffer << (Position.new(item))
    end
  end


  class Track
    include ErrorMessage
    attr_accessor :id, :pieces, :lanes

    def initialize(data)
      self.id = data['id']
      self.lanes = data['lanes']
      self.pieces = data['pieces']

      calculate_lengths

    end

    def lane_radius(piece_index, lane_index)
      piece = pieces[piece_index]
      lane = lanes[lane_index]

      piece['angle'] > 0 ? piece['radius'] - lane['distanceFromCenter'] : piece['radius'] + lane['distanceFromCenter']
    end

    # Assumes the track to be a closed loop.
    def next_curve(piece_index, in_piece_distance, lane)
      distance = -in_piece_distance
      (0..pieces.length).each do |i|
        index = (piece_index + i) % pieces.length
        piece = pieces[index]

        if i > 0 and piece['radius']

          push_error "Calculated distance: #{distance}, piece index:#{piece_index}, piece dist:#{in_piece_distance}, i: #{i}, index:#{index}" if distance < 0
          return {index: index, distance: distance}
        end

        distance += piece['length'] ? piece['length'] : piece[lane]
      end

      nil
    end

    def calculate_lengths
      pieces.each do |piece|
        if piece['radius']
          lanes.each_with_index do |lane, index|
            radius = piece['angle'] > 0 ? piece['radius'] - lane['distanceFromCenter'] : piece['radius'] + lane['distanceFromCenter']
            piece[index] = radius * PI * piece['angle'] / 180.0
          end
        end
      end
    end


  end

  class RaceSession
    attr_accessor :laps, :quick_race
    def initialize(data)
      self.laps = data['laps']
      self.quick_race = data['quickRace']
    end
  end

  class Car
    attr_accessor :color, :speed, :position_buffer, :speed_buffer

    def initialize(data, buffer_size)

      self.position_buffer = FixedSizedQueue.new buffer_size
      self.speed_buffer = FixedSizedQueue.new 3
      self.color = data['id']['color']
      self.speed = 0
    end

    def position
      self.position_buffer[0]
    end

    def previous_position
      self.position_buffer[1]
    end

  end

end
